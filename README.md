# Wav music player
A simple wav music player to store and play musics.

The music player is in a data structure
of an array used in a collection and iterators
and templates.

Do the following:
1) Add Folder - add folder to your data struct by giving only the name of the folder you want.

2) Add Song - add song to a folder you created (or by defulte it's in root ) by giving the name of the song the artist of the song and the lyrics and what folder(again optional).

3) print Folder Songs - prints the song inside the folder you specified by giving it folder name and artist name.

4) Print Artist Songs - print song by a given artist name. 
5) remove Folder - remove folder by a given folder name.
6) remove Song - remove song by a given song name and folder name. 
7) Move Song - move song from folder a to b by a given song name and target folder and suorce folder
8) Play Song - play song by a given song name and folder name

# Credit

Written by David levi and Shlomy Romano

Written to study c ++ applications

# How to insall

In order to start the player you have to run the MAKEFILE in the terminal and then run "./musicPlayer"
And this is what it's supposed to work for
Enjoy!!!
